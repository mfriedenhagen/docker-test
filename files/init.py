#!/usr/bin/env python
import logging
import os
import sys

def main(args):
    logging.debug("initial_args={}".format(args))
    logging.debug("pwd={}".format(os.getcwd()))
    cmd = args[1]
    logging.debug("cmd={}".format(cmd))
    remaining_args = args[1:]
    logging.debug("remaining_args={}".format(remaining_args))
    logging.debug("os.environ={}".format(os.environ))
    sys.stdout.flush()
    sys.stderr.flush()
    os.execvpe(cmd, remaining_args, os.environ)

if __name__ == "__main__":
    loglevel = os.environ.get("INIT_LOG_LEVEL", "INFO")
    logging.basicConfig(level=logging.getLevelName(loglevel))
    main(sys.argv)
