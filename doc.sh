#!/bin/sh -e
mkdir -p public
[ -z "$CI_BUILD_REF" ] && CI_BUILD_REF=${CI_BUILD_REF:-UNKNOWN} || CI_BUILD_REF=${CI_BUILD_REF:0:10}
CMD="asciidoctor -a project-version=$CI_BUILD_REF -o public/index.html -b html5 README.adoc"
[ -f /.dockerenv ] || CMD="docker run --rm -v $PWD:/documents -w /documents asciidoctor/docker-asciidoctor $CMD"
$CMD
