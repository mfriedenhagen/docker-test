FROM python:alpine
COPY files /
RUN mkdir -p /srv/config &&\
    mkdir -p /srv/data &&\
    mkdir -p /srv/log &&\
    chown daemon /srv/log &&\
    chown daemon /srv/data
ONBUILD COPY files /
ONBUILD RUN pip install -U pip &&\
            test -r requirements.txt && pip install -r requirements.txt
ONBUILD ENTRYPOINT ["/init.py"]
ONBUILD WORKDIR /srv
ONBUILD USER daemon
ONBUILD CMD ["/srv/app"]